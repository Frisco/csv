<?php

class _CSV {

    protected   $file,
                $handle,
                $options,
                $delimiter = "",
                $data = array(),
                $firstLineConfig = false;

    public function __construct($file, $options) {
        $this->file = $file;
        $this->handle = fopen($this->file, "r");
        $this->options = $options;

        $this->getDelimiter();
        $this->getFileContent();
    }

    protected function getDelimiter() {

        if ($this->options['delimiterAutoMode'] === 'enabled') {
            // Read the first line of the file and check if there is a statement that defines the delimiter
            $firstLineRead = fgets($this->handle);
            if (substr($firstLineRead, 0, 4) === "sep=") {
                // If there is a config line found, the delimiter is set to the appropriate character
                $this->delimiter = substr($firstLineRead, 4, 1);

                /**
                 * I need to check if the delimiter is a certain character, to prevent misuse.
                 * $validDelimiters = array(',', ';', '|')
                 * and check if the found delimiter character matches, otherwise refuse to set and return an error
                 */

                $this->firstLineConfig = true;
            } else {
                // If there is no config line found, the file handle pointer is set back to the first line
                rewind($this->handle);

                // Read the line and try to recognize the delimiter if it was not specified before
                $secondLineRead = fgets($this->handle);
                if (count(explode(",", $secondLineRead)) > 1) {
                    $this->delimiter = ",";
                } elseif (count(explode(";", $secondLineRead)) > 1) {
                    $this->delimiter = ";";
                } elseif (count(explode("|", $secondLineRead)) > 1) {
                    $this->delimiter = "|";
                } else {
                    // Delimiter could not be recognized, show error message
                }
            }

        } else {
            if ($this->options['delimiterHeadline'] === 'on') {

                $this->firstLineConfig = true;
                switch ($this->options['delimiter']) {
                    case "comma":
                        $this->delimiter = ",";
                        break;
                    case "semicolon":
                        $this->delimiter = ";";
                        break;
                    case "pipe":
                        $this->delimiter = "|";
                }
            }

        }

    }

    protected function getFileContent() {

        // Read the contents of the file line by line and pass them as array into an array
        foreach (file($this->file, FILE_IGNORE_NEW_LINES) as $line) {
            $this->data[] = str_getcsv($line, $this->delimiter);
        }

        // If the first line contains a "sep=DELIMITER", shift that array off and reorder the indexing of the array
        if ($this->firstLineConfig) {
            array_shift($this->data);
        }

    }

    public function getData() {
        return $this->data;
    }

}