<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CSV Import to DB</title>
    <style>
        .container { max-width: 400px; margin: 60px auto 0 auto; border: 1px solid #999; padding: 10px; }
    </style>
</head>
<body>
    <div class="container">
        <form method="POST" action="processFile.php" enctype="multipart/form-data">
            <input type="file" name="file" id="file">
            <br><br>
            <input type="checkbox" name="delimiterOptionsToggle" id="delimiterOptionsToggle" onclick="toggleDelimiterOptions()">
            <label for="delimiterOptionsToggle">Enable advanced Options</label>
            <fieldset id="delimiterOptions" disabled>
                <legend>Delimiter Options</legend>
                <input type="checkbox" name="delimiterHeadline" id="delimiterHeadline">
                <label for="delimiterHeadline" title="First line of File specifies the used Delimiter">File has a Delimiter Headline</label>
                <br>
                <select name="delimiter" id="delimiter">
                    <option selected value="comma">[ , ] Comma</option>
                    <option value="semicolon">[ ; ] Semicolon</option>
                    <option value="pipe">[ | ] Pipe</option>
                </select>
                <label for="delimiter">Content Delimiter</label>
            </fieldset>
            <br>
            <input type="checkbox" name="contentHeadline" id="contentHeadline">
            <label for="contentHeadline" title="File contains a Headline that contains the Headlines of the columns">File has a Content Headline</label>
            <br><br>
            <input type="submit" name="submit" value="Submit">
        </form>
    </div>
    <script src="js/script.js"></script>
</body>
</html>