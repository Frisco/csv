# README #

This was (is) an attempt at creating a library that can handly CSV files and process them. That means, extract their content and store it in a DB as well as create CSV files from a DB table.
I have started this many years ago for practicing purposes and haven't touched it in a while.
I'd like to start playing around with it again, but in case you are interested, just have a look if you find something interesting.

### What is this repository for? ###

* Handle CSV files

### How do I get set up? ###

* For now, you don't. It's not ready

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Repo owner or admin


## TODO ##
(Taken from the todo.txt file, no idea what has been done already)

* Get the File
* set Session Data for the File (add timestamp value to data and prepend to filename to allow multiple users upload files at the same time)
* check Post data, clean if needed
* check the File (delimiter, other options)
* store the File
* display the File
* finetune the settings for the DB (int, varchar, etc.)
* save data to DB
* delete the File