<?php

class Storage {

    public static $data = array();

    public static function storeData($key, $value) {
        self::$data[$key] = $value;
    }

    public static function retrieveData($key) {
        return self::$data[$key];
    }

}