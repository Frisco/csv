<?php

function debug($var, $scriptTerminator = false) {
    if (is_array($var) || is_object($var)){
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    } else {
        echo $var;
    }
    echo '<br>';
    if ($scriptTerminator) {
        exit();
    }
}